# Updated Portfolio for Kethlyn Africa, Ph.D.
**Developed By:** [Wanda McCrae](https://wandamccrae.com/)

**Date:** 17 October 2024


## Files

- **index.html:** Home page
- **about.html:** About page
- **styles.css:** CSS styling
- **docs:** Document files
- **image:** Image files
- **old-src:** Unused HTML files from the original site

## Updates

- Simplified the site to two pages
- Updated the nav bar to match
- Fixed the main container so it is centered to the page
- Made the footers sticky to the bottom of the page
